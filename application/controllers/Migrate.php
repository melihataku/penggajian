<?php

defined("BASEPATH") or exit("No direct script access allowed");
/*
 * Source Code Yii2 Advanced template Modification
 * Please contact our website for more information :
 * http://www.example.co.id/contact-us
 */

/**
 * Description of Migrate
 *
 * @author Andhy Taslin <taslin.webdeveloper@gmail.com>
 * @since 1.0
 */
class Migrate extends CI_Controller {

    public function index() {

        if (!$this->input->is_cli_request())
            exit('Execute via command line: php index.php migration');

        $this->load->library('migration');

        if (!$this->migration->latest())
            show_error($this->migration->error_string());
    }
//    public function drop($version = 0) {
//        if (!$this->input->is_cli_request())
//            exit('Execute via command line: php index.php migration');
//
//        $this->load->library('migration');
//        
//        if (!$this->migration->current($version))
//            show_error($this->migration->error_string());
//    }
}
