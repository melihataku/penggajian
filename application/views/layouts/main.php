<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title><?= $title ?></title>
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/ionicons.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/AdminLTE.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/skins/_all-skins.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/daterangepicker.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/datepicker3.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/css/style.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/select2/select2.min.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/datatables/dataTables.bootstrap.css">
		<link rel="stylesheet" href="<?= base_url() ?>/assets/datatables/jquery.dataTables.min.css">
		<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.slimscroll.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/app.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/demo.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/simple.money.format.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/moment.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/daterangepicker.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/js/bootstrap-datepicker.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/datatables/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="<?= base_url() ?>assets/datatables/dataTables.bootstrap.min.js"></script>
		<style>
			.box-border-radius{border-radius:5px;}
		</style>
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper">
			<?php $this->load->view('layouts/header', $params); ?>
			<?php $this->load->view('layouts/sidebar', $params); ?>
			<?= $content ?>
			<?php $this->load->view('layouts/footer', $params); ?>
		</div>
		<script type="text/javascript">
			$(document).ready(function () {
				$('#tabel').DataTable();
			});
			$(document).ready(function () {
				$('.uang').simpleMoneyFormat();
			});
			$(document).ready(function () {
				$('#tanggal_lahir').datepicker({
					autoclose: true
				});
			});
			$(document).ready(function () {
				$('.select').select2();
			});
			$(document).ready(function () {
				$('#periode').daterangepicker();
			});
			$(document).ready(function () {
				$(".setujui").click(function (e) {
					e.stopPropagation();
					e.preventDefault();
					if (confirm('Laporan ini akan disetujui, lanjutkan?')) {
						window.location.href = $(this).data('url');
					}
				});
			});
			function jumlahkan_gaji_bruto() {
				var gaji_pokok = document.getElementById('gaji_pokok').value;
				var tunjangan_suami_istri = document.getElementById('tunjangan_suami_istri').value;
				var tunjangan_anak = document.getElementById('tunjangan_anak').value;
				var gaji_bruto = parseInt(gaji_pokok) + parseInt(tunjangan_suami_istri) + parseInt(tunjangan_anak);
				if (!isNaN(gaji_bruto)) {
					document.getElementById('gaji_bruto').value = gaji_bruto;
				}
			};
			function jumlahkan_penghasilan_kotor() {
				var gaji_pokok = document.getElementById('gaji_pokok').value;
				var tunjangan_suami_istri = document.getElementById('tunjangan_suami_istri').value;
				var tunjangan_anak = document.getElementById('tunjangan_anak').value;
				var tunjangan_lauk_pauk = document.getElementById('tunjangan_lauk_pauk').value;
				var tunjangan_umum = document.getElementById('tunjangan_umum').value;
				var tunjangan_beras = document.getElementById('tunjangan_beras').value;
				var tunjangan_profesi = document.getElementById('tunjangan_profesi').value;
				var jumlah_penghasilan_kotor = parseInt(gaji_pokok) + parseInt(tunjangan_suami_istri) + parseInt(tunjangan_anak) + parseInt(tunjangan_lauk_pauk) + parseInt(tunjangan_umum) + parseInt(tunjangan_beras) + parseInt(tunjangan_profesi)
				var pph_ps_21 = parseInt(Math.ceil(jumlah_penghasilan_kotor * 2 / 100));
				if (!isNaN(jumlah_penghasilan_kotor)) {
					document.getElementById('jumlah_penghasilan_kotor').value = jumlah_penghasilan_kotor;
				}
				if (!isNaN(pph_ps_21)) {
					document.getElementById('pph_ps_21').value = pph_ps_21;
				}
			}; 
			function jumlahkan_potongan() {
				var iwp = document.getElementById('iwp').value;
				var bpjs = document.getElementById('bpjs').value;
				var utang = document.getElementById('utang').value;

				var gaji_pokok = document.getElementById('gaji_pokok').value;
				var tunjangan_suami_istri = document.getElementById('tunjangan_suami_istri').value;
				var tunjangan_anak = document.getElementById('tunjangan_anak').value;
				var tunjangan_lauk_pauk = document.getElementById('tunjangan_lauk_pauk').value;
				var tunjangan_umum = document.getElementById('tunjangan_umum').value;
				var tunjangan_beras = document.getElementById('tunjangan_beras').value;
				var tunjangan_profesi = document.getElementById('tunjangan_profesi').value;
				var jumlah_penghasilan_kotor = parseInt(gaji_pokok) + parseInt(tunjangan_suami_istri) + parseInt(tunjangan_anak) + parseInt(tunjangan_lauk_pauk) + parseInt(tunjangan_umum) + parseInt(tunjangan_beras) + parseInt(tunjangan_profesi)
				var pph_ps_21 = parseInt(Math.ceil(jumlah_penghasilan_kotor) * 2 / 100);
				var jumlah_potongan = parseInt(iwp) + parseInt(bpjs) + parseInt(pph_ps_21) + parseInt(utang);
				if (!isNaN(jumlah_potongan)) {
					document.getElementById('jumlah_potongan').value = jumlah_potongan;
				}
				var jumlah_penghasilan_bersih = parseInt(jumlah_penghasilan_kotor) - parseInt(jumlah_potongan)
				if (!isNaN(jumlah_penghasilan_bersih)) {
					document.getElementById('jumlah_penghasilan_bersih').value = jumlah_penghasilan_bersih;
				}
			};
		</script>
	</body>
</html>


