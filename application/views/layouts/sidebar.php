<aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>assets/img/user.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?= $sess['nama_depan'] . ' ' . $sess['nama_belakang'] ?></p>
          <?php if($sess) : ?>
            <i class="fa fa-circle text-success"></i> Online
          <?php endif; ?>
        </div>
      </div>
      <ul class="sidebar-menu">
        <li class="header"></li>
          <li>
            <a href="<?= base_url() ?>">
              <i class="fa fa-dashboard"></i> <span>Beranda</span>
            </a>
          </li>
        <?php if($sess['username'] == 'kasium' || $sess['username'] == 'admin') : ?>
          <li>
            <a href="<?= base_url('pembayaran_gaji') ?>">
              <i class="fa fa-th"></i> <span>Pembayaran Gaji</span>
            </a>
          </li>
          <?php elseif($sess['username'] == 'kasikeu' || $sess['username'] == 'admin') : ?>
          <li>
            <a href="<?= base_url('laporan') ?>">
              <i class="fa fa-th"></i> <span>Laporan Penerima Gaji</span>
            </a>
          </li>
        <?php endif;?>
        <li>
          <a href="<?= base_url('personil') ?>">
            <i class="fa fa-th"></i> <span>Personil</span>
            <?php if(isset($personil)) : ?>
              <?php if(date("d M Y", $personil['created_at']) == date("d M Y", time())) : ?>
                <span class="pull-right-container">
                  <small class="label pull-right bg-green">new</small>
                </span>
              <?php endif;?>
            <?php endif;?>
          </a>
        </li>
      </ul>
    </section>
</aside>