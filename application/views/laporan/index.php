<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Penggajian
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Pembayaran Gaji</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            <?php if($this->session->flashdata('msg_g')) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
                </div>
            <?php elseif($this->session->flashdata('msg')) : ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
                </div>
            <?php endif; ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Pembayaran Gaji</h3>
                </div>
                <div class="box-body">
                    <table id="tabel" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="col-lg-1">No.</th>
                                <th class="col-lg-1">Kode Bayar</th>
                                <th class="col-lg-3">Periode</th>
                                <th class="col-lg-1">Jumlah Personil</th>
                                <th class="col-lg-2">Total Gaji</th>
                                <th class="col-lg-1">Keterangan</th>
                                <th class="col-lg-3"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if ($pembayaran_gaji != FALSE) :
                                    foreach ($pembayaran_gaji as $key => $res) :
                                    ?>
                                        <tr>
                                            <td><?= $key+1 ?></td>
                                            <td><?= $res['kode_bayar_gaji'] ?></td>
                                            <td>Pembayaran gaji periode <?= date("d M Y", $res['periode_gaji_dari']) . '-' . date("d M Y", $res['periode_gaji_sampai'])?></td>
                                            <td><?= $res['jumlah_personil'] ?> orang</td>
                                            <td>Rp. <?= number_format($res['total_gaji'], 0,',', '.') ?></td>
                                            <td>
                                                <?php if($res['disetujui'] == 1) : ?>
                                                    <small class="label label-success"><i class="fa fa-check"></i> Sudah disetujui Kasikeu</small>
                                                <?php else: ?>
                                                    <small class="label label-warning"><i class="fa fa-clock-o"></i> Belum disetujui Kasikeu</small>
                                                <?php endif; ?>
                                                <?php if($res['slip_gaji_dibuat'] == 1) : ?>
                                                    <small class="label label-success"><i class="fa fa-check"></i> Slip gaji sudah dicetak</small>
                                                <?php else: ?>
                                                    <small class="label label-warning"><i class="fa fa-clock-o"></i> Slip gaji belum dicetak</small>
                                                <?php endif; ?>
                                            </td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= base_url('pembayaran_gaji/daftar_penerima_gaji/' . $res['id']) ?>">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Lihat
                                                </a>
                                                <?php if($res['disetujui'] == 2) : ?>
                                                    <a class="btn btn-info btn-sm setujui" data-url="<?= base_url('laporan/do_setujui/' . $res['id']) ?>">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Setujui
                                                    </a>
                                                <?php else: ?>
                                                    <a class="btn btn-info btn-sm disabled setujui" data-url="<?= base_url('laporan/do_setujui/' . $res['id']) ?>">
                                                        <i class="fa fa-file-pdf-o" aria-hidden="true"></i> Sudah Disetujui
                                                    </a>
                                                <?php endif; ?>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                endif;
                            ?>      
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>