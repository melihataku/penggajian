<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Penggajian
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url() ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
            <li class="active">Personil</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
            <?php if($this->session->flashdata('msg_g')) : ?>
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-ban"></i> <?= $this->session->flashdata('msg_g') ?></h5>
                </div>
            <?php elseif($this->session->flashdata('msg')) : ?>
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h5><i class="icon fa fa-check"></i> <?= $this->session->flashdata('msg') ?></h5>
                </div>
            <?php endif; ?>
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Personil</h3>
                </div>
                <div class="box-body">
                    <table id="tabel" class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th class="col-lg-1">No.</th>
                                <th class="col-lg-1">NRP</th>
                                <th class="col-lg-5">Nama Personil</th>
                                <th class="col-lg-2">Pangkat</th>
                                <th class="col-lg-2">Jabatan</th>
                                <th class="col-lg-1"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                if ($all_personil != FALSE) :
                                    foreach ($all_personil as $key => $res) :
                                    ?>
                                        <tr>
                                            <td><?= $key+1 ?></td>
                                            <td><?= $res['nrp'] ?></td>
                                            <td>
                                                <?= $res['nama_depan'] . ' ' . $res['nama_belakang'] ?>
                                                <?php if(date("d M Y", $res['created_at']) == date("d M Y", time())) : ?>
                                                    <span class="pull-right-container">
                                                        <small class="label bg-green">personil baru</small>
                                                    </span>
                                                <?php endif; ?>
                                                <?php if($res['status'] == 1) : ?>
                                                    <span class="pull-right-container">
                                                        <small class="label bg-green">aktif</small>
                                                    </span>
                                                <?php elseif($res['status'] == 2) : ?>
                                                    <span class="pull-right-container">
                                                        <small class="label bg-yellow">tidak aktif</small>
                                                    </span>
                                                <?php endif; ?>
                                            </td>
                                            <td><?= $res['nama_pangkat'] ?></td>
                                            <td><?= $res['nama_jabatan'] ?></td>
                                            <td>
                                                <a class="btn btn-info btn-sm" href="<?= base_url('personil/edit_personil/' . $res['nrp']) ?>">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </a>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                endif;
                            ?>      
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    <a class="btn btn-info btn-sm" href="<?= base_url('personil/tambah_personil/') ?>">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Tambah Baru
                    </a>
                </div>
            </div>
        </div>
    </section>
</div>