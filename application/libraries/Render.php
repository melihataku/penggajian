<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * Source Code Yii2 Advanced template Modification
 * Please contact our website for more information :
 * http://www.example.co.id/contact-us
 */

/**
 * Render Class untuk mempermudah render view dan layout
 *
 * @author Andhy Taslin <taslin.webdeveloper@gmail.com>
 * @since 1.0
 */
class Render {

    public $layout = 'main';

    public $layout_folder = 'layouts';

    public $layout_partial = 'partials';

    public $doctype = 'html5';

    public $lang = ' lang="en"';

    public $title;

    public $meta;

    public $content;

    public $link;

    public $script;

    public $javascript;

    private $data = [];

    private $CI = null;

    public function init () {
        $this->CI =& get_instance();
        $this->CI->load->library('parser');
        $this->CI->load->helper(array('html'));

        $this->data['doctype'] = doctype($this->doctype);
        $this->data['lang'] = $this->lang;
        $this->data['meta'] = meta($this->meta);
        $this->data['title'] = $this->title;

//        $link_output = null;
//        if (!empty($this->link) && $this->link !== NULL) {
//            if (is_array($this->link)) {
//                foreach ($this->link as $link) {
//                    $link = link_tag($link);
//                    $link_output .= <<<HTML
//        {$link}
//
//HTML;
//                }
//            }
//        }
//        $script_output = null;
//        if (!empty($this->script) && $this->script !== NULL) {
//            if (is_array($this->script)) {
//                foreach ($this->script as $script) {
//                    $script_tag = '<script src="{src}" type="text/javascript"></script>';
//                    $script_tag = strtr($script_tag, array('{src}' => $script));
//                    $script_output .= <<<HTML
//        {$script_tag}
//
//HTML;
//                    
//                }
//            }
//        }
//        $this->data['styles'] = $this->styles;
//        $this->data['script'] = $script_output;
//        $this->data['link'] = $link_output;
//        $this->data['javascript'] = $this->javascript;
    }
    public function data($views = NULL, $params = NULL, $use_parser = false) {
        $this->init();
        $view1 = ($use_parser) ? 'parser' : 'load';
        $view2 = ($use_parser) ? 'parse' : 'view';
        if(isset($params['title'])){
            $this->data['title'] = $params['title'];
        }
        $this->data['params'] = $params;
        $this->data['content'] = $this->CI->$view1->$view2($views, $this->data['params'], true);

        return $this->CI->$view1->$view2($this->layout_folder.'/'.$this->layout, $this->data);
    }
    public function partials($views = NULL, $params = NULL, $use_parser = false) {
        $this->init();
        $view1 = ($use_parser) ? 'parser' : 'load';
        $view2 = ($use_parser) ? 'parse' : 'view';

        return $this->CI->$view1->$view2($this->layout_partial.'/'.$views, $params, true);
    }
}
